from neural_module import neural_network as nn
import time
training_example, test_example = [], []

with open('data/optdigits.tra') as f:
    for line in f:
        training_example.append([int(x) for x in line.split(',')])

with open('data/optdigits.tra') as f:
    for line in f:
        test_example.append([int(x) for x in line.split(',')])

network = nn.NeuralNetwork(10, 50, training_example, 0.04)

start = time.time()
for i in range(10):
    print(i)
    network.train()

# test
true_cnt, cnt = 0, 0
for test in test_example:
    network.forward_propagation(test[:-1])

    min_error, min_num = 1000000, -1
    for i in range(len(network.output_layer.outputs)):
        if abs(1 - network.output_layer.outputs[i]) < min_error:
            min_error = abs(network.output_layer.outputs[i] - 1)
            min_num = i

    if min_num == test[-1]:
        true_cnt += 1
    cnt += 1

print("정확도 %.10f%%" %(true_cnt/cnt*100))
print("경과시간 %.2f" %(time.time()-start))




