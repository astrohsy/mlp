from neural_module import neural_network as nn

example = [[0, 1, 1], [1, 0, 1], [0, 0, 0], [1, 1, 0]]

network = nn.NeuralNetwork(2, 4, example, 0.5)

print("학습 전")
for test in example:
    network.forward_progation(test[:-1])
    print(test[:-1], network.output_layer.outputs)

for i in range(2000):
    network.train()

print("학습 후")
print("\n")
for test in example:
    network.forward_progation(test[:-1])
    print(test[:-1], network.output_layer.outputs)

