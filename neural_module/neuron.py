import random
import math


class Neuron:
    def __init__(self, in_prev_layer_num_of_neuron):
        self.synaptic_weights = [random.uniform(0.01, 0.03) for i in range(in_prev_layer_num_of_neuron)]

    def activate(self, inputs):
        evaluated_value = 0.0

        for i in range(self.synaptic_weights.__len__()):
            evaluated_value += inputs[i] * self.synaptic_weights[i]
        return Neuron.activation_function(evaluated_value)

    @staticmethod
    def activation_function(evaluated_value):
        return 1 / (1 + math.exp(-evaluated_value))