from neural_module import neural_layer as nl


class NeuralNetwork:
    def __init__(self, n_neuron_output_layer, n_neuron_hidden_layer, training_example, learning_rate=0.005):
        input_size = training_example[0].__len__() - 1
        self.output_layer = nl.Layer(n_neuron_hidden_layer, n_neuron_output_layer)
        self.hidden_layer = nl.Layer(input_size, n_neuron_hidden_layer)
        self.training_example = training_example
        self.learning_rate = learning_rate

    def forward_propagation(self, data_input):
        output_of_hidden_units = self.hidden_layer.evaluate(data_input)
        return self.output_layer.evaluate(output_of_hidden_units)

    def back_propagation(self, data_input, target):
        self.forward_propagation(data_input)

        error_k, error_h, = [], []
        for k in range(len(self.output_layer.outputs)):
            o_k = self.output_layer.outputs[k]
            error_k.append(o_k * (1 - o_k) * (target[k] - o_k))

        for h in range(len(self.hidden_layer.outputs)):
            o_h = self.hidden_layer.outputs[h]
            sigma = 0.0
            for k in range(len(self.output_layer.outputs)):
                sigma += self.output_layer.neurons[k].synaptic_weights[h] * error_k[k];
            error_h.append(o_h * (1 - o_h) * sigma)

        for j in range(len(self.output_layer.neurons)):
            for i in range(len(self.hidden_layer.outputs)):
                self.output_layer.neurons[j].synaptic_weights[i] += self.learning_rate * error_k[j] * \
                                                                    self.hidden_layer.outputs[i]

        for j in range(len(self.hidden_layer.neurons)):
            for i in range(len(data_input)):
                self.hidden_layer.neurons[j].synaptic_weights[i] += self.learning_rate * error_h[j] * data_input[i]

    def train(self):
        for example in self.training_example:
            true_val = [0 for i in range(len(self.output_layer.outputs))]
            true_val[example[-1]] = 1

            self.back_propagation(example[:-1], true_val)
