from neural_module import neuron as n


class Layer:
    def __init__(self, n_prev_neuron, n_neuron):
        self.neurons = [n.Neuron(n_prev_neuron) for i in range(n_neuron)]
        self.outputs = [0 for i in range(n_neuron)]

    def evaluate(self, data_input):
        for i, neuron in enumerate(self.neurons):
            self.outputs[i] = neuron.activate(data_input)

        return self.outputs
